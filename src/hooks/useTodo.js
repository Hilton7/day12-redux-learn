import { useRef } from "react"
import { useDispatch } from "react-redux"
import { loadTodos } from "../axios/todo"
import { addItemBYItemList } from "../slice/todoListSlice"
export const useTodo = () => {
  const dispatch = useDispatch()
  const reloadTodos = () => {
    loadTodos().then((res) => {
      console.log(res.data)
      dispatch(addItemBYItemList(res.data))
    })
  }
  return useRef({
    reloadTodos,
  }).current
}
