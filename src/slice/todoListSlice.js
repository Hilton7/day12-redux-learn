import { createSlice } from "@reduxjs/toolkit"

export const todoListSlice = createSlice({
  name: "todoList",
  initialState: {
    todoListItem: [],
  },
  reducers: {
    addItemBYItemList: (state, action) => {
      state.todoListItem = action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { addItemBYItemList } = todoListSlice.actions
export default todoListSlice.reducer
