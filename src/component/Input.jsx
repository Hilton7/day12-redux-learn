import { Button, Input, message } from "antd"
import { useState } from "react"
import { addTodo } from "../axios/todo"
import { useTodo } from "../hooks/useTodo"
import "./css/Input.css"
export default function () {
  const [messageApi, contextHolder] = message.useMessage()
  const [value, setValue] = useState("")
  const { reloadTodos } = useTodo()
  function handleChange(event) {
    setValue(event.target.value)
  }

  async function add() {
    if (value === undefined) return
    let val = value.trim()
    if (val == "") {
      messageApi.open({
        type: "error",
        content: "不能修改为空数据哦",
      })
      return
    }

    await addTodo({
      text: val,
      done: false,
    })
    reloadTodos()
    messageApi.open({
      type: "success",
      content: "增加成功",
    })
    setValue("")
  }
  return (
    <div className="input-box">
      {contextHolder}
      <Input
        placeholder="input..."
        allowClear
        onChange={handleChange}
        value={value}
        onPressEnter={(event) => {
          if (event.keyCode === 13) {
            add()
          }
        }}
        style={{ width: "45%" }}
      />
      <Button
        classNames={"button"}
        type={"primary"}
        onClick={add}
        style={{ backgroundColor: "#1E90FF", zIndex: "1" }}
      >
        add
      </Button>
    </div>
  )
}
