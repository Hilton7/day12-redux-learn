import { EditTwoTone } from "@ant-design/icons"
import { Input, Modal, message } from "antd"
import { useState } from "react"
import { deleteTodo, updateTodo, updateTodoTextById } from "../axios/todo"
import { useTodo } from "../hooks/useTodo"
import "./css/ListItem.css"
export default function ({ todoList }) {
  const [selectId, setSelectId] = useState("")
  const [selectText, setSelectText] = useState("")
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [messageApi, contextHolder] = message.useMessage()
  const { reloadTodos } = useTodo()
  let update = (index) => async () => {
    await updateTodo(todoList[index].id, !todoList[index].done)
    reloadTodos()
  }
  let cancel = (index) => async (event) => {
    event.stopPropagation()
    await deleteTodo(todoList[index].id)
    reloadTodos()
    messageApi.open({
      type: "success",
      content: "删除成功",
    })
  }
  function handleUpdate(id, text) {
    return function (event) {
      event.stopPropagation()
      setSelectId(id)
      setSelectText(text)
      setIsModalOpen(true)
    }
  }
  async function handleOk() {
    let text = selectText
    if (text.trim() == "") {
      messageApi.open({
        type: "error",
        content: "不能修改为空数据哦",
      })
      return
    }
    await updateTodoTextById(selectId, selectText)
    setIsModalOpen(false)
    reloadTodos()
    messageApi.open({
      type: "success",
      content: "修改成功",
    })
  }
  return (
    <div className="list-box">
      {contextHolder}
      {todoList.map((val, index) => {
        return (
          <div
            className={`list-box-item ${val.done ? "line" : ""}`}
            key={val.id}
            onClick={update(index)}
          >
            <img
              src={require("../static/delete.png")}
              onClick={cancel(index)}
            ></img>
            {val.text}
            <span className="edit" onClick={handleUpdate(val.id, val.text)}>
              <EditTwoTone />
            </span>
          </div>
        )
      })}
      <Modal
        title="Basic Modal"
        open={isModalOpen}
        cancelText={"cancel"}
        okTestText={"confirm"}
        onOk={handleOk}
        onCancel={() => {
          setIsModalOpen(false)
        }}
        keyboard={true}
        centered={true}
      >
        <Input
          placeholder="input..."
          allowClear
          onChange={(e) => {
            setSelectText(e.target.value)
          }}
          value={selectText}
        />
      </Modal>
    </div>
  )
}
