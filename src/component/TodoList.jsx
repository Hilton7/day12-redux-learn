import { useEffect } from "react"
import { useSelector } from "react-redux"
import { useTodo } from "../hooks/useTodo"
import Input from "./Input"
import ListItem from "./ListItem"
import "./css/TodoList.css"
export default function () {
  const todoList = useSelector((state) => state.todoListSlice.todoListItem)
  const { reloadTodos } = useTodo()
  useEffect(() => {
    reloadTodos()
  }, [])
  return (
    <div className="todo-list-box">
      <h4>To do List</h4>
      <ListItem todoList={todoList}></ListItem>
      <Input></Input>
    </div>
  )
}
