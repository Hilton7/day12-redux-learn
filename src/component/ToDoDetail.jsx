import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { getTodoById } from "../axios/todo"
const ToDoDetail = () => {
  const [item, setItem] = useState(null)
  const navigate = useNavigate()
  const params = useParams()
  const { id } = params
  const loadDetail = async () => {
    try {
      const res = await getTodoById(id)
      test = res.data
      console.log(res)
      setItem(res.data)
    } catch (error) {
      navigate("404")
    }
  }
  useEffect(() => {
    loadDetail()
  }, [])
  return (
    <div style={{ textAlign: "center" }}>
      <h4>Detail</h4>
      {item && <div className="done-list-item">{item.text}</div>}
    </div>
  )
}

export default ToDoDetail
