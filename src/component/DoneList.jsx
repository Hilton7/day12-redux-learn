import { useSelector } from "react-redux/es/hooks/useSelector"
import "./css/DoneList.css"
import { useNavigate } from "react-router-dom"
const DoneList = () => {
  const navigate = useNavigate()
  const todoList = useSelector((state) => state.todoListSlice.todoListItem)
  return (
    <div>
      <h4 style={{ textAlign: "center" }}>Done List</h4>
      <div>
        {todoList.map((todo) => {
          if (todo.done === true) {
            return (
              <div
                className="done-list-item"
                key={todo.id}
                onClick={() => {
                  navigate(`/todo/${todo.id}`)
                }}
              >
                {todo.text}
              </div>
            )
          }
        })}
      </div>
    </div>
  )
}

export default DoneList
