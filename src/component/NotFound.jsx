const NotFound = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <img src={require("../static/404.png")} style={{ width: "100%" }}></img>
    </div>
  )
}

export default NotFound
