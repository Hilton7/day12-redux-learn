import { configureStore } from "@reduxjs/toolkit"
import todoListSlice from "../slice/todoListSlice"
export default configureStore({
  reducer: { todoListSlice },
})
