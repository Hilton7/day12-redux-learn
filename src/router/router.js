import * as React from "react"
import { createBrowserRouter } from "react-router-dom"
import About from "../component/About"
import DoneList from "../component/DoneList"
import NotFound from "../component/NotFound"
import ToDoDetail from "../component/ToDoDetail"
import TodoList from "../component/TodoList"
import Layout from "../layout/Layout"
const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "about",
        element: <About />,
      },
      {
        path: "done",
        element: <DoneList />,
      },
      {
        path: "todoList",
        element: <TodoList />,
      },
      {
        path: "todo/:id",
        element: <ToDoDetail />,
      },
      {
        path: "*",
        element: <NotFound />,
      },
    ],
  },
  {
    path: "*",
    element: <NotFound />,
  },
])
export default router
