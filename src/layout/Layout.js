import { Outlet } from "react-router-dom"
import { NavLink } from "react-router-dom"
import "./layout.css"
const Layout = () => {
  return (
    <div>
      <div style={{ textAlign: "center",marginBottom:"15px" }}>
        <NavLink to="/todoList" className={"link"} >todoList</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <NavLink to="/about" className={"link"} >about</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <NavLink to="/done" className={"link"} >done</NavLink>
      </div>
      <Outlet />
    </div>
  )
}

export default Layout
