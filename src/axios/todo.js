import request from "./request";
export const loadTodos = () => {
  return request.get("/todos");
};
export const updateTodo = (id, done) => {
  return request.put(`/todos/${id}`, {
    done,
  });
};
export const deleteTodo = (id) => {
  return request.delete(`/todos/${id}`);
};

export const addTodo = (todo) => {
  return request.post(`/todos`, todo);
};
export const getTodoById = (id) => {
  return request.get(`/todos/${id}`);
};
export const updateTodoTextById = (id, text) => {
  return request.put(`/todos/${id}`, {
    text,
  });
};
