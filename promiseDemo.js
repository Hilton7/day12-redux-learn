new Promise((resolve, reject) => {
  console.log("do something")
  resolve(123)
}).then((value) => {
  console.log(value)
})

new Promise((resolve, reject) => {
  console.log("do something")
  reject(123)
}).catch((value) => {
  console.log(value)
})
